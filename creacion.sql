﻿/* codigo para general el esquema */

  -- base de datos
  DROP DATABASE IF EXISTS b20190529;
  CREATE DATABASE b20190529;

  /*
    tablas
  */

  -- cliente

  CREATE TABLE cliente(
    -- campos
    dni varchar(9),
    nombre varchar(100),
    apellidos varchar(100),    
    fechaNac date,
    tfno varchar(15),

    -- claves
    PRIMARY KEY (dni)
  );

  -- producto
  
    CREATE TABLE producto(
      -- campos
      codigo int AUTO_INCREMENT,
      nombre varchar(100) NOT NULL, -- requerido si
      precio float DEFAULT 0, -- valor por defecto

      -- claves
      PRIMARY KEY(codigo)
      );

  -- proveedor

    CREATE TABLE proveedor(
      -- campos
    dni varchar(9),
    nombre varchar(100),
    apellidos varchar(100),    
    fechaNac date,
    tfno varchar(15),

    -- claves
    PRIMARY KEY (dni)

    );

  -- compra
  
    CREATE TABLE compra (
      -- campos
      dniCliente varchar(9),
      codProducto int,

      -- clave
      PRIMARY KEY(dniCliente,codProducto),

      -- claves ajenas

      CONSTRAINT fkcompracliente FOREIGN KEY (dniCliente)
      REFERENCES cliente(dni),
      
      CONSTRAINT fkcompraproducto FOREIGN KEY (codProducto) 
      REFERENCES producto(codigo)

      );

    CREATE TABLE suministra(
      -- campos
      codProducto int,
      dniProveedor varchar(9),

      -- claves
      PRIMARY KEY(codProducto,dniProveedor),
      UNIQUE KEY(codProducto),

      -- clave ajena

      CONSTRAINT fksuministraproducto FOREIGN KEY (codProducto)
      REFERENCES producto(codigo),

      CONSTRAINT fksuministraproveedor FOREIGN KEY (dniProveedor)
      REFERENCES proveedor(dni)

      );

  



